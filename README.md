# Git репозиторий автообновлением

При коммите в ветку мастер содержимое ветки мастер должно обновлять папку /var/www

# Expected result:
- Ваш ubuntu сервер(*)
- Git репозиторий на сервере(*)
- Публичный репозиторий
- Склоненный с репозитория [https://github.com/MaksymSemenykhin/git-course-task/tree/task%233](https://github.com/MaksymSemenykhin/git-course-task/tree/task%233)
- Процедура будет работать с прод репой.
- Не использовать облачные git сервисы.
- Не использовать облачные teamcity, jenkins... сервисы.
- В README файле описано детали настройки и flow с этим репозиторием.
- Репозиторий может содержать доп скрипты гит репозитория.
- Доступ на сервер для ключа task3.pub без пароля.
 
=======
# Краткое описание
Продакшн репозиторий : /srv/git/devops.git
Рабочая директория : /home/teamcity/git/git-course-task-3
копируем все папки в которых встречаются файлы с расширениями .css,.html,.jpg,.js,.php

Workflow:
- обновляем рабочую директорию с прод репы
- создаем контент в папке /home/teamcity/git/git-course-task-3
- делаем коммиты в прод репе
- заливаем изменения в прод репу
- файлы должны автоматически скопироваться в папку /var/www

# Необходимые настройки:
1. в папке hooks prod репы создаем файл post-receive:

    #!/bin/bash
    unset GIT_INDEX_FILE
    export GIT_WORK_TREE=/var/www
    export GIT_DIR=/srv/git/devops.git
    git checkout -f

    меняем права на файл:
    sudo chmod +x post-receive

2. в папке info prod репы создаем файл sparse-checkout и прописываем 
в нем регулярки для отбора файлов для check-out согласно требованиям

    /**/*.html
    /**/*.css
    /**/*.js
    /**/*.php
    /**/*.jpg
    /**/*.png

3. делаем sparse-checkout активной в локальной конфигурации прод репозитория:

git config core.sparsecheckout true

--- Далее идет детальное описание подготовки и проверки решения.

# The detailed action definition

git clone https://github.com/MaksymSemenykhin/git-course-task.git ~/git/git-course-task-3
cd ~/git/git-course-task-3
git co task#3
git remote add prod teamcity@localhost:/srv/git/devops.git
git fetch prod task#3
git pull prod master

# task#3 prepairing conditions - create content
# source state: /home/teamcity/git/git-course-task-3
├── README.md
├── css
│   ├── test.css
│   └── test.txt
├── html2
│   ├── test.html
│   └── test.txt
├── img
│   ├── test.jpg
│   └── test.txt
├── index.php
├── js
│   ├── test.js
│   └── test.txt
├── php
│   ├── test.php
│   └── test.txt
├── task3.pub
├── test.html
├── test.txt
├── test2.txt
├── test3.txt
├── test4.txt
└── test5.txt

#create post-commit
[teamcity@ip-172-31-23-181] /srv/git/devops.git/hooks (master) $ cat post-receive
#!/bin/bash
unset GIT_INDEX_FILE
export GIT_WORK_TREE=/var/www
export GIT_DIR=/srv/git/devops.git
git checkout -f

#create sparse-checkout
[teamcity@ip-172-31-23-181] /srv/git/devops.git/info (master) $ cat ./sparse-checkout
/**/*.html
/**/*.css
/**/*.js
/**/*.php
/**/*.jpg
/**/*.png
/**/*.pub

#source state of /var/www
[teamcity@ip-172-31-23-181] /var/www $ tree /var/www
/var/www
├── html
│   └── index.html
├── nodes
│   ├── dev
│   └── master
├── pborodin
│   └── html
│       ├── Lab1
│       │   ├── js
│       │   │   ├── lab1_1.js
│       │   │   └── lab1_2.js
│       │   └── php
│       │       ├── lab1_1.php
│       │       └── lab1_2.php
│       ├── css
│       │   └── index.css
│       ├── index.html
│       ├── lab1_1.html
│       ├── lab1_2.html
│       ├── lab1_js.html
│       └── lab1_php.html
└── teamcity

# The commit changes
git push prod task#3:master

# The results of commit
[teamcity@ip-172-31-23-181] /var/www $ tree /var/www
/var/www
├── css
│   └── test.css
├── html
│   └── index.html
├── html2
│   └── test.html
├── img
│   └── test.jpg
├── js
│   └── test.js
├── nodes
│   ├── dev
│   └── master
├── pborodin
│   └── html
│       ├── Lab1
│       │   ├── js
│       │   │   ├── lab1_1.js
│       │   │   └── lab1_2.js
│       │   └── php
│       │       ├── lab1_1.php
│       │       └── lab1_2.php
│       ├── css
│       │   └── index.css
│       ├── index.html
│       ├── lab1_1.html
│       ├── lab1_2.html
│       ├── lab1_js.html
│       └── lab1_php.html
├── php
│   └── test.php
├── teamcity
└── test.html
